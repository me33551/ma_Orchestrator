# ma_Orchestrator

After cloning the repository, the "localVersion" html files should be adapted so that the paths to the javascript libraries are correct (after downloading the required libraries).

Afterwards, the files from the [Visualiser](https://gitlab.com/me33551/ma_Visualiser) project need to be linked into this directory because they are needed for the visualisation.

Finally, the website needs to be hosted on a webserver (e.g., nginx on localhost) and can be started - in order to use the functionality, the [EventProvider](https://gitlab.com/me33551/ma_EventProvider) and the [ExtractorAggregatorViolationFunction](https://gitlab.com/me33551/ma_ExtractorAggregatorViolationFunction) projects need to be cloned and started.